<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    function responseText($text, $status)
    {
        return response()->json([
            "status" => $text
        ], $status);
    }

    function login(Request $request)
    {
        $user = new User;

        $email_re = $request->email;
        $password_re = $request->password;

        if ($email_re && $password_re) {

            $theuser = $user->where('email', $email_re)->first();

            if ($theuser) {

                $password_db = $theuser->password;

                if (md5($password_re) == $password_db) {

                    $theuser->accessToken = md5($theuser->email);

                    if ($theuser->save()) {
                        return response()->json([
                            "accessToken" => $theuser->accessToken,
                            "data" => [
                                "email" => $theuser->email,
                                "username" => $theuser->username,
                                "mobile" => $theuser->mobile
                            ]
                        ]);
                    }
                    return $this->responseText("unable to login user", 400);
                }
                return $this->responseText("password provided is incorrect", 400);

            }
            return $this->responseText("user does not exist, please create an account", 400);
        }
        return $this->responseText("please provide email and password", 400);
    }

    function register(Request $request)
    {

        $user = new User;

        $email_re = $request->email;
        $password_re = $request->password;
        $username_re = $request->username;
        $mobile_re = $request->mobile;

        if ($email_re && $username_re && $password_re && $mobile_re) {

            $theuser = $user->where('email', $email_re)->first();

            if (!$theuser) {

                $user->email = $email_re;
                $user->password = md5($password_re);
                $user->username = $username_re;
                $user->mobile = $mobile_re;

                if ($user->save()) {
                    return $this->responseText("user has successfully registered", 200);
                }
                return $this->responseText("unable to register user", 400);
            }
            return $this->responseText("user currently exists", 400);
        }
        return $this->responseText("please enter parameters", 400);
    }

    function logout(Request $request)
    {
        $user = new User;

        $accessToken = $request->header('accessToken');
        $theuser = $user->where('accessToken', $accessToken)->first();

        $theuser->accessToken = null;

        if ($theuser->save()) {
            return $this->responseText("user has successfully logout", 400);
        }
        return $this->responseText("unable to logout user", 400);
    }

    function updateUser(Request $request)
    {
        $user = new User;

        $accessToken = $request->header('accessToken');
        $theuser = $user->where('accessToken', $accessToken)->first();

        $password_re = $request->password;
        $username_re = $request->username;
        $mobile_re = $request->mobile;

        if ($password_re || $username_re || $mobile_re) {
            if ($password_re) {
                $theuser->password = $password_re;
            }
            if ($username_re) {
                $theuser->username = $username_re;
            }
            if ($mobile_re) {
                $theuser->mobile = $mobile_re;
            }

            if ($theuser->save()) {
                return $this->responseText("profile update successful", 200);
            }
            return $this->responseText("unable to update profile", 400);
        }

        return $this->responseText("please enter parameters", 400);
    }

    function deleteUser(Request $request)
    {
        $user = new User;

        $email_re = $request->email;

        if($email_re) {

            $email = $user->where('email', $email_re)->first();

            if($email) {
                if($email->role == 'user') {
                    if($email->delete()) {
                        return $this->responseText("user deletion successful", 200);
                    }
                    return $this->responseText("unable to delete user", 400);
                }
                return $this->responseText("you can't delete an admin, silly!", 400);
            }
            return $this->responseText("user does not exist", 400);
        }

        return $this->responseText("please enter user email to delete", 400);
    }
}
