<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = new User;
        $accessToken = $request->header('accessToken');
        $role = $user->where('accessToken', $accessToken)->first()->role;

        if ($role == 'admin') {
            return $next($request);
        }
        return response()->json([
            "status" => "access forbidden"
        ], 403);
    }
}
