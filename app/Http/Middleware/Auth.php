<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = new User;
        $accessToken = $request->header('accessToken');

        if($accessToken) {

            $theuser = $user->where('accessToken', $accessToken)->first();

            if($theuser) {
                return $next($request);
            }

            return response()->json([
                "status" => "unauthorized access, user not logged in"
            ], 401);
        }

        return response()->json([
            "status" => "please provide an access token"
        ], 400);
    }
}
